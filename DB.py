import numpy as np
import pandas as pd
def distance(DB):    
    dist_matrix = np.zeros([len(DB), len(DB)])
    for i in range(len(DB)):
        for j in range(len(DB)):
            dist_matrix[i,j] = dist(np.array(DB.iloc[i, :2]), np.array(DB.iloc[j,:2]))
    return dist_matrix

def RangeQuery(DB, distance, Q, eps):
    Neighbors = []
    for point in np.arange(len(distance)):
        if distance[point, Q] <= eps:
            Neighbors.append(point)
    return Neighbors
    
def dist(a, b):
    return np.linalg.norm([a - b])


def DBSCAN(DB, d, minPts, eps):
    C = 0
    for p in np.arange(len(DB.iloc[:,0])):
        
        if DB.iloc[p,2] > -1:
            continue
        N = RangeQuery(DB, d, p, eps)
        if len(N) < minPts:
            DB.iloc[p, 2] = 0
            continue
        C += 1
        DB.iloc[p, 2] = C
        S = N
        S.remove(p)
        S_index = 0
        while S_index < len(S) :
            q = S[S_index]
            if DB.iloc[q, 2] == 0:
                DB.iloc[q, 2] = C
            if DB.iloc[q, 2] > -1:
                S_index += 1
                continue
            DB.iloc[q, 2] = C
            N = RangeQuery(DB, d, q, eps)
            if len(N) >= minPts:
                t = S
                #print(t)
                t += N
                # print(t)
                # print(type(t))
                S = set(t)
                S = list(S)
            S_index += 1
    return DB
          

def main():
    import numpy
    import matplotlib.pyplot as pyplot
    #kmeans parameters:
    #number of clusters:
    k=2
    #variance
    var = 1
    #generating distributions:
    #first real mean, x axis
    mean1x = 0.5
    #first mean, y axis
    mean1y = -0.5
    #same for second means
    mean2x = -3
    mean2y = -3
    num_points = 100

    a = numpy.random.multivariate_normal([mean1x, mean1y], [[var, 0], [0, var]], size=num_points)
    b = numpy.random.multivariate_normal([mean2x, mean2y], [[var, 0], [0, var]], size=num_points)

    # pyplot.plot(a[:,0], a[:,1],'ro')
    # pyplot.plot(b[:,0], b[:,1],'bo')
    # pyplot.show()

    import pandas as pd
    import numpy as np
    df1 = pd.DataFrame({'x':a[:,0],'y':a[:,1], 'label':-np.ones(len(a))})
    df2 = pd.DataFrame({'x':b[:,0],'y':b[:,1], 'label':-np.ones(len(b))})
    DB = pd.concat([df1 ,df2])
    distance_matrix = distance(DB)
    DB2 = DBSCAN(DB, distance_matrix, 5, 1)
    print(DB2)

if __name__ == "__main__":
    main()